{-# OPTIONS_GHC -fno-warn-unused-imports #-}
{-# OPTIONS_GHC -fno-warn-unused-matches #-}
{-# OPTIONS_GHC -fno-warn-unused-top-binds #-}

module CapDL.DynDL
    ( printDynDL
    , toDynDL
    , dynDL
    ) where

import qualified CapDL.Model as C
import CapDL.DynDL.Print (printDynDL)
import CapDL.DynDL.Transform (toDynDL)

dynDL :: C.Model Word -> String
dynDL model@(C.Model _ objects _ _ _) =
    printDynDL (toDynDL model)
